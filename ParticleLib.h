//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "setup/App.h"

#ifndef __PARTICLELIB_H__
#define __PARTICLELIB_H__

#include "pd_api.h"

class ParticleLib {
public:
    static const lua_reg* metaTable();
    static LCDBitmap* flake(int index);
};

#endif
