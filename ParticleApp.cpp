//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "ParticleLib.h"

//#include <stdio.h>
//#include <stdlib.h>
#include <math.h>

class ParticleApp: public App {

public:
    ParticleApp(PlaydateAPI* pd): App(pd, false) {}

    virtual int handleInitLua();
};

int ParticleApp::handleInitLua() {
    
    const char* err;
    
    shared()->playdateAPI()->system->logToConsole("%s:%i: in init_particlesLib", __FILE__, __LINE__);

    if ( !shared()->playdateAPI()->lua->registerClass("particlelib.particles", ParticleLib::metaTable(), NULL, 0, &err) ) {
        shared()->playdateAPI()->system->logToConsole("%s:%i: registerClass failed, %s", __FILE__, __LINE__, err);
        return -1;
    }

    srand(shared()->playdateAPI()->system->getSecondsSinceEpoch(NULL));
    return 0;
}

// Provide this somewhere in your app
void App::makeApp(PlaydateAPI* pd) {
    App::_shared = new ParticleApp(pd);
}
