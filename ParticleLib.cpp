//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//
//
//  main.c
//  Extension
//
//  Created by Dan Messing on 5/01/18.
//  Copyright (c) 2018 Panic, Inc. All rights reserved.
//

#include "ParticleLib.h"
#include "ParticleBag.h"
#include "setup/App.h"
#include "pd_api.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

static int argInt(int index) {
    return App::shared()->playdateAPI()->lua->getArgInt(index);
}
static void* argObj(int index, const char* libName) {
    return App::shared()->playdateAPI()->lua->getArgObject(index, (char*)libName, NULL);
}
static void pushInt(int i) {
    App::shared()->playdateAPI()->lua->pushInt(i);
}
static void pushObj(void* obj, const char* libName) {
    App::shared()->playdateAPI()->lua->pushObject(obj, (char*)libName, 0);
}
static LCDBitmap* loadBitmap(const char *path, const char **outErr) {
    return App::shared()->playdateAPI()->graphics->loadBitmap(path, outErr);
}

static ParticleBag* getParticlesArg(int index) {
    return (ParticleBag*)(argObj(index, "particlelib.particles"));
}

static int s_newobject(lua_State* L) {
    ParticleBag* p = new ParticleBag(argInt(1));
    pushObj(p, "particlelib.particles");
    return 1;
}

static int s_dealloc(lua_State* L) {
    ParticleBag* p = getParticlesArg(1);
    delete p;
    return 0;
}

static int s_setNumberOfParticles(lua_State* L) {
    getParticlesArg(1)->setCount(argInt(2));
    return 0;
}

static int s_particleCountInRect(lua_State* L) {
    
    LuaUDObject* obj;
    App::shared()->playdateAPI()->lua->getArgObject(2, (char*)"playdate.geometry.rect", &obj);
    int result = getParticlesArg(1)->particleCountInRect(*(PDRect*)obj);
    pushInt(result);
    return 1;
}

static int s_update(lua_State* L) {
    getParticlesArg(1)->update();
    return 0;
}
static int s_draw(lua_State* L) {
    getParticlesArg(1)->draw(App::shared()->playdateAPI()->graphics);
    return 0;
}

static const lua_reg particlesLib[] = {
    { "__gc",                    s_dealloc },
    { "new",                     s_newobject },
    { "setNumberOfParticles",    s_setNumberOfParticles },
    { "particleCountInRect",     s_particleCountInRect },
    { "update",                  s_update },
    { "draw",                    s_draw },
    { NULL, NULL }
};

static LCDBitmap* flakes[4];

const lua_reg* ParticleLib::metaTable(void) {
    
    // load particle images
    const char *outErr = NULL;
    flakes[0] = loadBitmap("images/snowflake1", &outErr);
    flakes[1] = loadBitmap("images/snowflake2", &outErr);
    flakes[2] = loadBitmap("images/snowflake3", &outErr);
    flakes[3] = loadBitmap("images/snowflake4", &outErr);
    return &particlesLib[0];
}

LCDBitmap* ParticleLib::flake(int index) {
    return flakes[index];
}
