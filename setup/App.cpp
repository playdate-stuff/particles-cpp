//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "App.h"


#ifdef _WINDLL
__declspec(dllexport)
#endif
#if __cplusplus
extern "C"
#endif
int eventHandler(PlaydateAPI* pd, PDSystemEvent event, uint32_t arg) {
    
    if (App::shared() == NULL) {
        App::makeApp(pd);
    }
    return App::shared()->dispatch(event, arg);
}

App* App::_shared = NULL;

App* App::shared() {
    return _shared;
}

int App::update_callback(void* userdata) {
    
    return ((App*)userdata)->update();
}

App::App(PlaydateAPI* pd, bool registerUpdater) {
    
    this->pdapi = pd;
    
    // Note: If you set an update callback in this handler, the system assumes the game is pure C and doesn't run any Lua code in the game
    if (registerUpdater)
        pd->system->setUpdateCallback(App::update_callback, (void*)this);
}

int App::dispatch(PDSystemEvent event, uint32_t arg) {
    
    pdapi->system->logToConsole("%s:%i: dispatch event %d, arg %ul", __FILE__, __LINE__, event, arg);
    
    switch (event) {
            
        case kEventInit:
            return handleInit();
            break;
            
        case kEventInitLua:
            return handleInitLua();
            break;
            
        case kEventLock:
            return this->handleLock();
            break;
            
        case kEventUnlock:
            return this->handleUnlock();
            break;
            
        case kEventPause:
            return this->handlePause();
            break;
            
        case kEventResume:
            return this->handleResume();
            break;
            
        case kEventTerminate:
            return this->handleTerminate();
            break;
            
        case kEventKeyPressed: // arg is keycode
            return this->handleKeyPressed(arg);
            break;
            
        case kEventKeyReleased:
            return this->handleKeyReleased();
            break;
            
        case kEventLowPower:
            return this->handleLowPower();
            break;
            
        default:
            pdapi->system->logToConsole("%s:%i: unknown event %d in dispatch", __FILE__, __LINE__, event);
            break;
    }
    
    return 0;
}

int App::update() { return 1; }

int App::handleInit() { return 0; }
int App::handleInitLua() { return 0; }
int App::handleLock() { return 0; }
int App::handleUnlock() { return 0; }
int App::handlePause() { return 0; }
int App::handleResume() { return 0; }
int App::handleTerminate() { return 0; }
int App::handleKeyPressed(uint32_t arg) { (void)arg; return 0; }
int App::handleKeyReleased() { return 0; }
int App::handleLowPower() { return 0; }
