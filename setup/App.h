//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "pd_api.h"

#ifndef __APP_H__
#define __APP_H__

class App {
    
private:
    static App* _shared;
    static int update_callback(void* userdata);
    PlaydateAPI* pdapi;

public:
    static App* shared();
	
	static void makeApp(PlaydateAPI*);

    App(PlaydateAPI* pd, bool registerUpdater);

    PlaydateAPI* playdateAPI() { return pdapi; }

	int dispatch(PDSystemEvent event, uint32_t arg);

	// Subclasses may override the remainder of these, and need not call super
	
	virtual int update();
	
	virtual int handleInit();
	virtual int handleInitLua();
	virtual int handleLock();
	virtual int handleUnlock();
	virtual int handlePause();
	virtual int handleResume();
	virtual int handleTerminate();
	virtual int handleKeyPressed(uint32_t arg);
	virtual int handleKeyReleased();
	virtual int handleLowPower();
};

#endif // __APP_H__
