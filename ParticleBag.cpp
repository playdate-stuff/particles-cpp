//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//
//
//  main.c
//  Extension
//
//  Created by Dan Messing on 5/01/18.
//  Copyright (c) 2018 Panic, Inc. All rights reserved.
//

#include "ParticleBag.h"
#include "ParticleLib.h"
#include "setup/App.h"
#include "pd_api.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <numeric>

Particle::Particle() {
    x = (rand() % 441) - 20.0f;
    y = (rand() % 241) - 240.0f;
    w = 19;
    h = 21;
    type = rand() % 4;
    speed = rand() % 4 + 1;
    drift = (float)((rand() % 5) - 2.0) / 10.0f;
}

bool Particle::isIn(int x, int y, int w, int h) const {
    return !(this->y >= y+h || this->y + this->h <= y || this->x >= x+w || this->x + this->w <= x);
}

ParticleBag::ParticleBag(size_t count) {
    this->particles = std::vector<Particle>(count);
}

void ParticleBag::setCount(size_t count) {
    this->particles.resize(count);
}


struct Accumulator {
    int x, y, w, h;
    Accumulator(int _x, int _y, int _w, int _h): x(_x), y(_y), w(_w), h(_h)  { }
    Accumulator(PDRect rect): x((int)rect.x), y((int)rect.y), w((int)rect.width), h(int(rect.height))  { }
    int operator()(int a, const Particle& b) {return a + (int)b.isIn(x, y, w, h);}
};

int ParticleBag::particleCountInRect(int x, int y, int w, int h) {
    return std::accumulate(this->particles.begin(), this->particles.end(), 0, Accumulator(x, y, w, h));
}

int ParticleBag::particleCountInRect(PDRect rect) {
    return std::accumulate(this->particles.begin(), this->particles.end(), 0, Accumulator(rect));
}

void ParticleBag::update() {
    
    for (auto& particle : this->particles) {
        particle.drift += (float)(((rand() % 5) - 2.0f) / 10.0f);
        particle.y += particle.speed;
        particle.x += particle.drift;

        if ( particle.y > 240 ) {   // if the particle is off the screen, move it back to the top
            particle.y = -22;
            particle.x = (rand() % 441) - 20;
            particle.drift = (float)((rand() % 5) - 2.0f) / 10.0f;
        }
    }
}


void ParticleBag::draw(const struct playdate_graphics* g) {
    g->clear(kColorBlack);

    for (auto const particle : this->particles) {
        g->setDrawMode(kDrawModeInverted);
        g->drawBitmap(ParticleLib::flake(particle.type), round(particle.x), particle.y, kBitmapUnflipped);
    }
}
