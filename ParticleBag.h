//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "setup/App.h"
#include "pd_api.h"
#include <vector>

#ifndef __PARTICLEBAG_H__
#define __PARTICLEBAG_H__

struct Particle {
    float x;
    int y;
    int w;
    int h;
    int speed;
    float drift;
    int type;
    
    Particle();
    
    bool isIn(int x, int y, int w, int h) const;
} ;

struct ParticleBag {
    
    std::vector<Particle> particles;
        
    ParticleBag(size_t count);
    
    void setCount(size_t count);
    int particleCountInRect(int x, int y, int w, int h);
    int particleCountInRect(PDRect rect);
    void update(void);
    void draw(const struct playdate_graphics* g);
};

#endif
